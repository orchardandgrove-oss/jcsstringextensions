//
//  JCSStringExtensions.swift
//
//  Created by Josh Wisenbaker on 10/20/18.
//  Copyright © 2018 Jamf. All rights reserved.
//
import Foundation

extension String {

    /// Format a base64 string so that it can be used in a URI.
    ///
    /// The returned `String` has three conversions applied to it.
    /// - "`+`" is converted to "`=`"
    /// - "`/`" is converted to "`_`"
    /// - Instances of "`=`" are deleted.
    /// - Returns: A `String` that is a converted copy of the original.
    public func base64URL() -> String {
        var temp = self
        temp = temp.replacingOccurrences(of: "+", with: "-")
        temp = temp.replacingOccurrences(of: "/", with: "_")
        temp = temp.replacingOccurrences(of: "=", with: "")
        return temp
    }

    /// Sanatize the memory used for a `String`.
    ///
    /// Method will deallocate `String` storage, overwrite it with
    /// two `UUID`s, then empty and deallocate it again.
    public mutating func sanatize() {
        self.removeAll()
        self = UUID().uuidString + UUID().uuidString
        self.removeAll()
    }

    /// Trim any extra whitespace off the head and tail of a `String`
    ///
    /// - Returns: A `String` that has all of it's leading and trailing whitespace removed.
    public func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
