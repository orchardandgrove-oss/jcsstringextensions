import XCTest

import JCSStringExtensionsTests

var tests = [XCTestCaseEntry]()
tests += JCSStringExtensionsTests.allTests()
XCTMain(tests)