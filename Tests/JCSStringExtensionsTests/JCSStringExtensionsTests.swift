import XCTest
@testable import JCSStringExtensions

final class StringExtensionTests: XCTestCase {
    func testSanatize() {
        var sut = "This is a secret"
        sut.sanatize()
        XCTAssert(sut.isEmpty, "String should be empty but is not.")
    }

    func testBase64URL() {
        var sut = "+/="
        let simpleString = sut.base64URL()
        XCTAssert(simpleString == "-_", "String was not cleaned.")
        sut = "fj33JJh+jfjfjf22!!/ff="
        let complexString = sut.base64URL()
        XCTAssert(complexString == "fj33JJh-jfjfjf22!!_ff", "Complex string was not cleaned properly.")
    }

    func testTrim() {
        let sut = "  this string has too much space     "
        let trimmedSut = sut.trim()
        XCTAssert(trimmedSut == "this string has too much space", "String was not trimmed properly")
    }

    static var allTests = [
        ("testSanatize, testBase64URL", "testTrim", testSanatize, testBase64URL, testTrim),
        ]
}

